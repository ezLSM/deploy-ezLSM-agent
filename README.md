# Ansible: Deploy ezLSM Agent!

[![Status](https://img.shields.io/badge/status-complete-brightgreen.svg?style=plastic)](#)
[![Platform](https://img.shields.io/badge/platform-CentOS%206-lightgrey.svg?style=plastic)](#)
[![GPLv3](https://img.shields.io/badge/license-GPLv3-blue.svg?style=plastic)](https://www.gnu.org/licenses/gpl.html)

This is an Ansible playbook that deploys the ezLSM agent on a CentOS 6 server as part of the ezLSM capstone project. This playbook is designed to be used with ansible-pull and run locally on the server. 

# Prerequisites

Ansible v2.0 is required to use this repo. To install Ansible, see [here](https://docs.ansible.com/ansible/intro_installation.html#installing-the-control-machine).

A Vault token is required to obtain a signed certificate from the Vault server. The Vault token is located in `/root/.client-certs-token.yml` on the Vault server.

This playbook uses a role-based policy to determine which configuration settings to apply on the server (iptables, rsyslog, splunk forwarder, audit, etc). Policies are defined in `custom_policies/<server_role>.yml`. The playbook looks for the role definition in `/root/.server_role`.

# Roles

This playbook consists of multiple roles:

1. common
    - installs basic packages required by later stages of the playbook
2. rsyslog
    - configures rsyslog to collect and redirect security logs for comsumption by a [Splunk forwarder](https://www.splunk.com/en_us/download/universal-forwarder.html)
3. iptables
    - configures host-based firewall rules
4. auditd
    - configures the audit subsystem
5. psad
    - installs and configures [psad](http://cipherdyne.org/psad/) to detect port scan and other attacks
6. splunkforwarder
    - installs and configures a [Splunk forwarder](https://www.splunk.com/en_us/download/universal-forwarder.html) to collect local logs and send them to a Splunk server
7. ossec-client
    - installs and configures a OSSEC client and automatically enrolls with the OSSEC server
8. monit
    - installs and configures [monit](https://www.mmonit.com/monit/) for automated process monitoring

# Usage

This playbook is designed to be run autonomously by the remote server using ansible pull:

    $ ansible-pull -U git@gitlab.com:ezLSM/deploy-ezLSM-agent.git local.yml -i inventory --purge --full --accept-host-key

## Deploying the playbook

1. fork this repo: 

    `$ git clone https://gitlab.com/ezLSM/deploy-ezLSM-agent.git`
2. configure settings in `group_vars/all`  
3. commit changes
